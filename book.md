# Fifty Dollar Battle Bots

Fifty Dollar battle bots is my attempt at a battle bots competition, but with a twitst, Robots must be build for $50 or less.

The Fifty Dollar battle bots league will operate in South Dakota with participants from neighboring states.

## Robot Rules
Robots must be constructed entire out of parts that can be bought for $50 or less. For a part to be purchased you must be able to get a recipt or an invoce for it's purchase. A list of recipts and invoices must be compiled and given to referees to certify that your robot was built for $50 or less.

Robot must not weigh more than 10 pounds.

### Explosives
Robots may use up to 1 ounce (28.35 grams). As a general rule, if it is illegal, please do not put it on your robot.

### Safety
While almost any materials may be used to build your robot, som e materials have limitations for safety reasons.
- No compressed flamable gasses are allowed (example: propane gas)
- No volatile chemicals may be used (example: hydrofluoric acid, liquid oxygen)
- Lasers
- Projectile weapons with a range over 10 feet.

### Allowed Weapons
These are just things that are intented to be used as weapons, but judges may still deem weapons of this type unsafe.

- Liquid fuel flamethrowers (example: gasoline or alcohol)
- Electrostatic Weapons (example: tesla coil, ESD gun)
- Blunt force weapons (example: hammer)

## Game Rules
The game is to be played in a 20 foot diameter circle, known as the "Arena". There must also be 10 feet of space around the arena, for safety.

The game begins with two robots on opposite sides of the field, and when the referee starts the game, the robots may move. The goal of the game is to push your opponent's robot out of the arena. The primary way of doing this is to disable the other robot first, then push them out.

### Safety
- Robot weapons must have a range less than 10 feet.
